import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeModule } from './home/home.module';
import { PublicModule } from './public/public.module';

import { NotFoundComponent } from './not-found/not-found.component';


@NgModule({
  imports: [
    CommonModule,
    HomeModule,
    PublicModule,
  ],
  declarations: [
 
    NotFoundComponent,
  ]
})
export class PagesModule { }
