<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;

class AdminPermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $adminRole = Role::findByName('admin');
        $adminRole->givePermissionTo('create_subject');
        $adminRole->givePermissionTo('edit_subject');
        $adminRole->givePermissionTo('delete_subject');
        $adminRole->givePermissionTo('get_subject');
    }
}
