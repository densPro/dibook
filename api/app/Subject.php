<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subject extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'description', 'estimatedTime', 'icon'
    ];

    public function users() {
        return $this->belongsToMany('App\User', 'users_subjects', 'subject_id', 'user_id');
    }

    public function themes()
    {
        return $this->hasMany('App\Theme');
    }
}
