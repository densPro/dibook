import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';

import { ComponentsModule } from '../../components/components.module';
import { HomeRoutingModule } from './home.routing';

import { HomeComponent } from './home.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { SubjectComponent } from './subject/subject.component';
import { SubjectCardComponent } from './subject-card/subject-card.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    
    ComponentsModule,
    HomeRoutingModule,
  ],
  declarations: [
    HomeComponent, 
    DashboardComponent, 
    UserProfileComponent,
    SubjectComponent,
    SubjectCardComponent
  ]
})
export class HomeModule { }
