<?php

namespace App\Http\Controllers;

use App\Http\Repositories\RepositoryWithRelationships;
use App\Subject;

class SubjectController extends CRUDController
{
    public function __construct()
    {
        parent::__construct( new RepositoryWithRelationships(Subject::class));
    }

    public function index()
    {
        return response()->json([
            'data' => $this->repository->with('users')->get()
        ]);
    }

    public function show($id)
    {
        return response()->json([
            'data' => $this->repository->with('themes')->find($id)
        ]);
    }
}
