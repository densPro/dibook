<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;

class TrainerPermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $trainerRole = Role::findByName('trainer');
        $trainerRole->givePermissionTo('get_subject');
        $trainerRole->givePermissionTo('edit_subject');
    }
}
