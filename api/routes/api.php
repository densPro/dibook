<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => 'cors'], function () {
    Route::post('/login','Auth\LoginController@check');
});

Route::group(['middleware' => ['cors','auth.basic']], function () {
    Route::resource('/subjects','SubjectController', ['only' => [
        'index', 'show'
    ]]);
    Route::resource('/users','UserController', ['only' => [
        'update'
    ]])->middleware('user_filter');
});

Route::group(['middleware' => ['cors','auth.basic','role:admin']], function () {
    Route::resource('/users','UserController', ['only' => [
        'store', 'destroy', 'index', 'show'
    ]]);
});

Route::group(['middleware' => ['cors','auth.basic','role:trainer|admin']], function () {
    //Route::get('/users', 'UserController@index');
//    Route::get('notes/{id}/destroy', 'NotesController@destroy')->name('notes.destroy');
    Route::resource('/subjects','SubjectController', ['only' => [
        'store', 'update', 'destroy'
    ]]);
});

/*Route::group(['middleware' => ['cors','auth.basic','role:trainer']], function () {
    Route::get('/subjects','SubjectController@index');
});*/

Route::get('/logout', function () {
    Auth::logout();
    Session::flush();
})->middleware('auth.basic');
