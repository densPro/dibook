import { Component, OnInit } from '@angular/core';
import { Subject } from '../../../models/subject';
import { APP_SETTINGS } from '../../../resources/url-paths';
import { HttpClientService } from '../../../services/http-client.service';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  private subjects; 
  
  constructor(private httpService: HttpClientService) {
    this.subjects = new Array();
  }

  ngOnInit() {
    this.httpService
    .get(APP_SETTINGS.API_ENDPOINT + 'subjects')
    .subscribe(
    response => {
      this.subjects = response.data;
    });
  }

  goToSubject(){
  }

}
