import { Injectable } from '@angular/core';

import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';


@Injectable()
export class HttpClientService {
  constructor(private http: Http) { }

  createAuthorizationHeader(headers: Headers) {
    let user: any = JSON.parse(localStorage.getItem('user'));
    let password: any = JSON.parse(localStorage.getItem('password'));    
    headers.append("Authorization", "Basic " + btoa(user.email + ":" + password));
    headers.append("Content-Type", "application/x-www-form-urlencoded");
    headers.append("Content-Type", "application/json");
  }

  get(url) {
    console.log(url);
    let headers = new Headers();
    this.createAuthorizationHeader(headers);
    return this.http
      .get(url, {
        headers: headers
      })
      .map(this.extractData)
      .catch(this.handleError);
  }

  post(url, data) {
    console.log(url);
    console.log(data);
    let headers = new Headers();
    this.createAuthorizationHeader(headers);
    return this.http.post(url, data, {
      headers: headers
    })
      .map(this.extractData);
  }

  postlogin(url, data) {
    console.log(url);
    let headers = new Headers();
    let email = sessionStorage.getItem('user_email');
    let password = sessionStorage.getItem('password');
    headers.append("Authorization", "Basic " + btoa(email + ":" + password));
    headers.append("Content-Type", "application/x-www-form-urlencoded");
    headers.append("Content-Type", "application/json");    
    
    return this.http.post(url,JSON.stringify(data), {
      headers: headers
    })
      .map(this.extractData);
  }

  postWithoutHeader(url, data) {
    let headers = new Headers(); 
    headers.append("Content-Type", "application/json");        
    return this.http.post(url, JSON.stringify(data), {
      headers : headers
    })
      .map(this.extractData);
  }

  put(url, data) {
    console.log(url);
    let headers = new Headers();
    this.createAuthorizationHeader(headers);
    return this.http.put(url, data, {
      headers: headers
    })
      .map(this.extractData);
  }

  delete(url, data) {
    console.log(url);
    let headers = new Headers();
    let body = JSON.stringify(data);
    let options = new RequestOptions(
      {
        headers: headers,
        body: body
      });
    this.createAuthorizationHeader(headers);
    return this.http
      .delete(url, options)
      .map(this.extractData);
  }

  getWithoutHeaders(url) {
    console.log(url);
    return this.http
      .get(url)
      .map(this.extractData)
      .catch(this.handleError);
  }

  private extractData(res: Response) {
    let body = res.json();
    return body;
  }

  private handleError(error: Response | any) {
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    console.error(errMsg);
    return Observable.throw(errMsg);
  }
}