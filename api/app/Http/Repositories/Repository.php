<?php

namespace App\Http\Repositories;

class Repository implements IRepository
{
    protected  $model;
    public function __construct($model)
    {
        $this->model = $model;
    }

    public function all()
    {
        return $this->model::all();
    }

    public function find($id)
    {
        return  $this->model::find($id);
    }

    public function create(array $attributes)
    {
        return $this->model::create($attributes);
    }

    public function update(array $attributes, $id)
    {
        $element = $this->model::find($id);
        $element->update($attributes);
        return $element;
    }

    public function delete($id)
    {
        return $this->model::destroy($id);
    }
}