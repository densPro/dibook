<?php

namespace App\Http\Repositories;


class RepositoryWithRelationships extends Repository  implements IRepositoryWithRelationships
{
    protected $relations;

    public function __construct($model)
    {
        parent::__construct($model);
        //$this->relations = $model::$DEFAULT_RELATIONS;
    }

    function with($relations){
        return $this->model::with($relations);
    }

   /* function allWithRelations()
    {
        //return $this->model::all()->load( $this->model::$DEFAULT_RELATIONS);
        return $this->model::all()->load($this->relations);
    }

    function findWithRelations($id)
    {
        return $this->model::with($this->relations)->find($id);
    }*/
}