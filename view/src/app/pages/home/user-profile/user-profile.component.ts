import { Component, OnInit } from '@angular/core';

import { APP_SETTINGS } from '../../../resources/url-paths';
import { HttpClientService } from '../../../services/http-client.service';

declare var $: any;


@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css']
})
export class UserProfileComponent implements OnInit {

  private user;

  constructor(private httpService: HttpClientService) {
  }

  ngOnInit() {
    this.user = JSON.parse(localStorage.getItem('user'));
    this.removeIsEmpty();
  }

  removeIsEmpty() {
    for (let element in this.user) {
      if(this.user[element] !== null){
        $('#' + element).parent().removeClass("is-empty");    
      }
    }
  }

  update(){
    this.httpService
    .put(APP_SETTINGS.API_ENDPOINT + 'users/' + this.user.id, this.user)
    .subscribe(
    response => {
      let user = response.data;
      user['password'] = this.user.password;
      this.user = user;
      localStorage.setItem('user', JSON.stringify(this.user)); 
      $.notify("Perfil Actualizado");
    });
  }
}
