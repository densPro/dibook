<?php

namespace App\Http\Controllers;

use App\Http\Repositories\Repository;
use App\Theme;

class ThemeController extends CRUDController
{
    public function __construct()
    {
        parent::__construct( new Repository(Theme::class));
    }
}
