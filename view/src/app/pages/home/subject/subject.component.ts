import { Component, OnInit } from '@angular/core';
import { Subject } from '../../../models/subject';
import { APP_SETTINGS } from '../../../resources/url-paths';
import { HttpClientService } from '../../../services/http-client.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-subject',
  templateUrl: './subject.component.html',
  styleUrls: ['./subject.component.css']
})
export class SubjectComponent implements OnInit {

  private subject;
  constructor(private httpService: HttpClientService, private route: ActivatedRoute) {
    this.subject = new Array();
  }

  ngOnInit() {
    this.route.params.subscribe(params =>
      this.subject.id = params['id']
    );
    this.httpService
      .get(APP_SETTINGS.API_ENDPOINT + 'subjects/' + this.subject.id)
      .subscribe(
      response => {
        this.subject = response.data;
        console.log(this.subject);
      });
  }

}
