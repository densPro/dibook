import { Component, OnInit, Output } from '@angular/core';
import { User } from '../../../models/user';
import { APP_SETTINGS } from '../../../resources/url-paths';
import { HttpClientService } from '../../../services/http-client.service';

declare var $: any;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  private user: User;
  constructor(private httpService: HttpClientService) {
  }

  ngOnInit() {
    this.user = new User();
  }

  signIn() {
    this.httpService
      .postWithoutHeader(APP_SETTINGS.API_ENDPOINT + 'login', this.user)
      .subscribe(
      response => {
        localStorage.setItem('password',this.user.password);
        this.user = response.data;
        localStorage.setItem('user', JSON.stringify(this.user));        
        window.location.replace('#/home/subjects');
      },
      error => {
        $.notify({
          title: '<strong>Error</strong>',
          message: 'El usuario o la contraseña es invalida',
        }, {
            type: 'danger'
        });
      });
  }
}
