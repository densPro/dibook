<?php
namespace App\Http\Repositories;

/**
 * Interface RepositoryInterface
 * @package Prettus\Repository\Contracts
 */
interface IRepository{

    function all();
    function find($id);
    function update(array $attributes, $id);
    function create(array $attributes);
    function delete($id);
}