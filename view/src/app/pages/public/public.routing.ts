import { NgModule } from '@angular/core';
import { CommonModule, HashLocationStrategy, LocationStrategy } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { Routes, RouterModule } from '@angular/router';

import { PublicComponent } from './public.component';
import { LoginComponent } from './login/login.component';
import { SignupComponent } from './signup/signup.component';
import { AboutUsComponent } from './about-us/about-us.component';
// import { NotFoundComponent } from './pages/not-found/not-found.component';


const routes: Routes = [
    {
        path: 'public', component: PublicComponent,
        children: [
            { path: 'login', component: LoginComponent },
            { path: 'signup', component: SignupComponent },
            { path: 'about-us', component: AboutUsComponent },
        ]
    }

];

@NgModule({
    imports: [
        CommonModule,
        BrowserModule,
        RouterModule.forRoot(routes)
    ],
    providers: [
        { provide: LocationStrategy, useClass: HashLocationStrategy }
    ],
    exports: [
        RouterModule
    ],
})
export class PublicRoutingModule { }

