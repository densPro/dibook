import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ComponentsModule } from '../../components/components.module';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { PublicRoutingModule } from './public.routing';

import { PublicComponent } from './public.component';
import { LoginComponent } from './login/login.component';
import { SignupComponent } from './signup/signup.component';
import { AboutUsComponent } from './about-us/about-us.component';
import { HttpClientService } from '../../services/http-client.service';


@NgModule({
  imports: [
    CommonModule,
    RouterModule,

    ComponentsModule,    
    PublicRoutingModule,
    FormsModule,
    HttpModule
  ],
  declarations: [
    PublicComponent,
    LoginComponent, 
    SignupComponent,
    AboutUsComponent
  ],
  providers: [
    HttpClientService,
  ]
})
export class PublicModule { }
