<?php

use Illuminate\Database\Seeder;
use App\Subject;

class SubjectsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $subject = Subject::create([
            'name' => str_random(14),
            'description' => str_random(10),
            'estimatedTime' => rand(1,999),
            'icon'=> 'web'
        ]);
        $idTrainerDanna = 2;
        $subject->users()->attach($idTrainerDanna);
    }
}
