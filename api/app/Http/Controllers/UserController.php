<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Repositories\Repository;

class UserController extends CRUDController
{
    public function __construct()
    {
        parent::__construct( new Repository(User::class));
    }
}
