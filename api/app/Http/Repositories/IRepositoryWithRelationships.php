<?php
/**
 * Created by PhpStorm.
 * User: denspro
 * Date: 26-01-18
 * Time: 10:25 PM
 */

namespace app\Http\Repositories;


interface IRepositoryWithRelationships extends IRepository
{
    function with($relations);
}