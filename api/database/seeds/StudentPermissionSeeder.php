<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;

class StudentPermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $studentRole = Role::findByName('student');
        $studentRole->givePermissionTo('get_subject');
    }
}
