<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Permission::create(['name' => 'create_subject']);
        Permission::create(['name' => 'edit_subject']);
        Permission::create(['name' => 'delete_subject']);
        Permission::create(['name' => 'get_subject']);
    }
}
