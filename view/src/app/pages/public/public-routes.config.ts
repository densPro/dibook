import {  RouteInfo } from '../../resources/metadata';

export const ROUTES: RouteInfo[] = [
    { path: 'login', title: 'Iniciar sesión',  icon: 'fingerprint', class: '' },
    { path: 'signup', title: 'Registrarse',  icon:'person_add', class: '' },
    { path: 'about-us', title: 'Dibook',  icon:'group_work', class: '' }
];