import {  RouteInfo } from '../../resources/metadata';

export const ROUTES: RouteInfo[] = [
    { path: 'subjects', title: 'Tutoriales',  icon: 'subject', class: '' },
    { path: 'user-profile', title: 'Perfil de Usuario',  icon:'person', class: '' },
    /*{ path: 'typography', title: 'Typography',  icon:'library_books', class: '' },
    { path: 'icons', title: 'Icons',  icon:'bubble_chart', class: '' },
    { path: 'maps', title: 'Maps',  icon:'location_on', class: '' },
    { path: 'notifications', title: 'Notifications',  icon:'notifications', class: '' },
    { path: 'upgrade', title: 'Upgrade to PRO',  icon:'unarchive', class: 'active-pro' }, */
];
