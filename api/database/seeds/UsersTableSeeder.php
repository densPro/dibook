<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $adminUser = User::create([
            'name' => 'sebas',
            'last_name' => 'Salazar',
            'email' => 'sebas@gmail.com',
            'password' => Hash::make('1234')
        ]);
        $trainerUser = User::create([
            'name' => 'Dannae',
            'last_name' => 'Aguilar',
            'email' => 'dannae@gmail.com',
            'password' => Hash::make('1234')
        ]);
        $studentUser = User::create([
            'name' => 'denis',
            'last_name' => 'Parra',
            'email' => 'denis@gmail.com',
            'password' => Hash::make('1234')
        ]);
        $adminUser->assignRole('admin');
        $trainerUser->assignRole('trainer');
        $studentUser->assignRole('student');
    }
}
